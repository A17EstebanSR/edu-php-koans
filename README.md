PHP-Koans
=========

### Para qué es este repo? ###

Aprende PHP arreglando pruebas fallidas. Diseñado como una serie de problemas sucesivos en los que escribe piezas faltantes de código para aprender conceptos básicos de sintaxis con PHP. Cuando haya terminado, también tendrá una biblioteca completa de ejemplos de código a los que también podrá hacer referencia.

Diseñado para enseñar los conceptos básicos de PHP e inspirado en el absolutamente increíble [http://www.rubykoans.com](http://www.rubykoans.com) 


### Pre requisitos

* Instalar PHP 5.3 o superior y la posibilidad de ejecutar scripts ```php``` desde consola de comandos.
* Instalar Composer desde [https://getcomposer.org](https://getcomposer.org) para gestionar las dependencias necesarias.

## Antes de ponerte a trabajar...

#### Haz un fork del repositorio original

Haz un fork del repositorio original y **configúralo de forma privada** (la actividad propuesta es individual o en grupo cerrado de alumnos según se indique ;)

Invita al profesor con permiso de escritura (Write).

#### Instala las dependencias del proyecto

Abre la terminal. Sitúate en la carpeta de tu proyecto y ejecuta  ```composer install```. Esta herramienta satisfará las dependencias necesarias para ejecutar el proyecto.

#### Ejecuta el proyecto 

Ejecuta ```php koans.php``` desde la terminal y sigue las instrucciones. Vuelve a ejecutar este comando hasta que tododos los ejercicios/problemas estén solucionados hasta que indique `ALL DONE`

#Exercises

##Chapter 1 - Variables
1. [Declaration](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter1.php#L12) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter1.php#L20)]
2. [Integers](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter1.php#L26) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter1.php#L35)]
3. [Floats](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter1.php#L40) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter1.php#L50)]
4. [Booleans](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter1.php#L54) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter1.php#L65)]
5. [Null](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter1.php#L68) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter1.php#L80)]
6. [Casting](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter1.php#L82) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter1.php#L95)]
7. [Assignment](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter1.php#L96) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter1.php#L110)]
8. [Constants](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter1.php#L110) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter1.php#L125)]

##Chapter 2 - Math
1. [Addition](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter2.php#L12) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter2.php#L21)]
2. [Subtraction](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter2.php#L27) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter2.php#L38)]
3. [Multiplication](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter2.php#L43) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter2.php#L54)]
4. [Division](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter2.php#L58) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter2.php#L69)]
5. [Modulus](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter2.php#L72) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter2.php#L85)]
6. [Rounding Naturally](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter2.php#L87) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter2.php#L101)]
7. [Rounding Up and Down](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter2.php#L102) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter2.php#L118)]
8. [Increment and Decrement](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter2.php#L116) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter2.php#L134)]

##Chapter 3 - Strings
1. [Concatenation](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter3.php#L12) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter3.php#L21)]
2. [Substitution](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter3.php#L27) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter3.php#L37)]
3. [Replacement](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter3.php#L42) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter3.php#L53)]
4. [Length](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter3.php#L57) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter3.php#L69)]
5. [Single Characters](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter3.php#L72) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter3.php#L85)]
6. [Substrings](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter3.php#L87) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter3.php#L103)]
7. [Changing Case](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter3.php#L104) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter3.php#L123)]
8. [Searching](https://github.com/mtoigo/PHP-Koans/blob/master/Chapters/Chapter3.php#L121) - [[S](https://github.com/mtoigo/PHP-Koans/blob/solutions/Chapters/Chapter3.php#L143)]

#Status
I'm building this out as a side project when I have time so the exercises don't cover all of the concepts they should yet, but everything in the master branch will always be complete.

#Feedback
If you found these helpful, spot a bug, or want to help contribute please [contact me](http://www.matt-toigo.com/contact).